<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class BookTitle extends DB{
    public $id="";

    public $booktitle="";

    public $author_name="";


    public function __construct(){



        parent::__construct();

    }

    public function setData($data=NULL)
    {


        if (array_key_exists('id', $data)) {


            $this->id = $data['id'];
        }

        if (array_key_exists('booktitle', $data)) {


            $this->booktitle = $data['booktitle'];
        }

        if (array_key_exists('author_name', $data)) {


            $this->author_name= $data['author_name'];
        }
    }
    public function store()
    {
        $arrData = array($this->booktitle, $this->author_name);

        $sql = "INSERT INTO book_title ( booktitle, author_name) VALUES ( ?, ?)";

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute($arrData);

        if ($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Inserted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');
    }










        public function index($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from book_title');


        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

// end of index()

    public function view($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from book_title where id=' . $this->id);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;

    }// end of view()


    public function update(){

        $arrData  = array($this->booktitle,$this->author_name);

        $sql = 'UPDATE book_title  SET booktitle  = ?   , author_name = ? where id ='.$this->id;

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()


    public function delete(){

        $sql = "DELETE FROM book_title WHERE id =".$this->id;

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');


    }// end of delete()



}